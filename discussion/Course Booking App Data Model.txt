
Course Booking App Data

====USERS====

{
	_id: objectId,
	firstName:string,
	lastName:string,
	email: string;
	password: string,
	isAdmin: boolean,
	mobileNo: string,
	enrollments: [
	{
		courseID: string,
		courseName: string,
		enrolledOn: date
	}
	]
}



====Courses====

{
	_id: objectId,
	name: string,
	Description: string,
	price: number.
	isActive: Boolean,
	slots: number,
	enrollees:[
	{
		userID: objectId,
		enrolledOn: date
	}
	],
	createdOn: date
}


===Transactions===

{
	_id: objectId,
	userID: objectId,
	courseID: objectId,
	totalAmount; number,
	isPaid: boolean,
	paymentMethod: string,
	dateTimeCreated: date
}


DATA MODEL DESIGN

	the main consideration for the structure of your documents



====Embedded Data Model====
Company Data Model (branches is embedded)
{
	"_id": objectId,
	"name": "ABC Company",
	"contact": "09123456789",
	"branches": [
	{
		"name": "branch1",
		"address": "Quezon City",
		"contact": "09987645321"
	}
	]
}


====Reference Data Model===
Company Data Model
{
	"_id": objectId,
	"name": "ABC Company",
	"contact": "09123456789",
}

Branches Data Model
{
	"_id": objectId,
	"companyId": objectId, // the id coming from the company data model
	"name": "branch1",
		"address": "Quezon City",
		"contact": "09987645321"
}